/*
 *   Copyright 2013 by Martin Gräßlin <mgraesslin@kde.org>

 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU Library General Public License as
 *   published by the Free Software Foundation; either version 2, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details
 *
 *   You should have received a copy of the GNU Library General Public
 *   License along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
#include "model.h"
#include <kwindoweffects.h>
#include <kwindowsystem.h>

#include <QQuickItem>
#include <QQuickWindow>

// magic number of Effects supported by KWindowEffects
static const int sg_effectCount = 7;

Model::Model(QObject* parent)
    : QAbstractItemModel(parent)
{
}

Model::~Model()
{
}

int Model::columnCount(const QModelIndex& parent) const
{
    Q_UNUSED(parent)
    return 1;
}

int Model::rowCount(const QModelIndex& parent) const
{
    if (parent.isValid()) {
        return 0;
    }
    return sg_effectCount;
}

QModelIndex Model::parent(const QModelIndex& child) const
{
    Q_UNUSED(child)
    return QModelIndex();
}

QModelIndex Model::index(int row, int column, const QModelIndex& parent) const
{
    if (column != 0 || parent.isValid()) {
        return QModelIndex();
    }
    if (row < 0 || row >= sg_effectCount) {
        return QModelIndex();
    }
    return createIndex(row, column);
}

QHash< int, QByteArray > Model::roleNames() const
{
    QHash<int, QByteArray> roleNames;
    roleNames[Qt::DisplayRole] = QByteArrayLiteral("name");
    roleNames[Qt::UserRole] = QByteArrayLiteral("available");
    return roleNames;
}


QVariant Model::data(const QModelIndex& index, int role) const
{
    if (!index.isValid()){
        return QVariant();
    }
    if (role == Qt::DisplayRole) {
        return indexToName(index.row());
    } else if (role == Qt::UserRole) {
        return QVariant(indexToEffect(index.row()));
    }

    return QVariant();
}

QString Model::indexToName(int index) const
{
    switch (index) {
    case 0:
        return QStringLiteral("Slide");
    case 1:
        return QStringLiteral("WindowPreview");
    case 2:
        return QStringLiteral("PresentWindows");
    case 3:
        return QStringLiteral("PresentWindowsGroup");
    case 4:
        return QStringLiteral("HighlightWindows");
    case 5:
        return QStringLiteral("BlurBehind");
    case 6:
        return QStringLiteral("Dashboard");
    default:
        return QString();
    }
}

bool Model::indexToEffect(int index) const
{
    switch (index) {
    case 0:
        return KWindowEffects::isEffectAvailable(KWindowEffects::Slide);
    case 1:
        return KWindowEffects::isEffectAvailable(KWindowEffects::WindowPreview);
    case 2:
        return KWindowEffects::isEffectAvailable(KWindowEffects::PresentWindows);
    case 3:
        return KWindowEffects::isEffectAvailable(KWindowEffects::PresentWindowsGroup);
    case 4:
        return KWindowEffects::isEffectAvailable(KWindowEffects::HighlightWindows);
    case 5:
        return KWindowEffects::isEffectAvailable(KWindowEffects::BlurBehind);
    case 6:
        return KWindowEffects::isEffectAvailable(KWindowEffects::Dashboard);
    default:
        return false;
    }
}

void Model::presentWindowsAll(QQuickItem *item)
{
    KWindowEffects::presentWindows(item->window()->winId());
}

void Model::presentWindowsCurrent(QQuickItem *item)
{
    KWindowEffects::presentWindows(item->window()->winId(), KWindowSystem::currentDesktop());
}

void Model::highlightWindows(QQuickItem *controller, bool enable)
{
    QList<WId> windows;
    if (enable) {
        windows << controller->window()->winId();
    }
    KWindowEffects::highlightWindows(controller->window()->winId(), windows);
}

void Model::blurBehind(QQuickItem* item, bool enable)
{
    KWindowEffects::enableBlurBehind(item->window()->winId(), enable);
}

void Model::markAsDashboard(QQuickItem* item)
{
    KWindowEffects::markAsDashboard(item->window()->winId());
}

void Model::slideWindow(QQuickItem *item, SlideFromLocation location, int offset)
{
    KWindowEffects::slideWindow(item->window()->winId(), static_cast<KWindowEffects::SlideFromLocation>(location), offset);
}

void Model::thumbnail(QQuickItem *controller, QQuickItem *window, const QRect &rect)
{
    QList<WId> windows;
    if (window) {
        windows << window->window()->winId();
    }
    QList<QRect> rects;
    if (!rect.isNull()) {
        rects << rect;
    }
    KWindowEffects::showWindowThumbnails(controller->window()->winId(), windows, rects);
}
