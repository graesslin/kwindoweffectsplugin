/*
 *   Copyright 2013 by Martin Gräßlin <mgraesslin@kde.org>

 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU Library General Public License as
 *   published by the Free Software Foundation; either version 2, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details
 *
 *   You should have received a copy of the GNU Library General Public
 *   License along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
import QtQuick 2.0;
import org.kde.kwindoweffects 1.0;
import org.kde.plasma.core 2.0 as PlasmaCore;
import org.kde.plasma.components 2.0 as PlasmaComponents;

Item {

    KWindowEffects {
        id: effects
    }
    PlasmaCore.Dialog {
        id: dialog
        visible: dialogButton.checked
        mainItem: Item {
            id: dialogItem
            width: 200
            height: 200
        }
    }

    ListView {
        id: effectsList
        anchors {
            top: parent.top
            right: parent.right
            left: parent.left
            bottom: buttons.top
        }
        model: effects
        delegate: Text {
            text: name + ": " + (available ? "yes" : "no")
        }
    }
    PlasmaComponents.ButtonColumn {
        id: buttons
        anchors {
            bottom: parent.bottom
            right: parent.right
            left: parent.left
        }
        PlasmaComponents.ButtonRow {
            PlasmaComponents.Button {
                id: dialogButton
                text: "Show Dialog"
                checkable: true
            }
            PlasmaComponents.Button {
                checkable: true
                checked: true
                text: "Blur Dialog"
                onClicked: {
                    if (checked && !dialogButton.checked) {
                        dialogButton.checked = true;
                    }
                    effects.blurBehind(dialogItem, checked)
                }
            }
            PlasmaComponents.Button {
                text: "Mark Dialog as Dashboard"
                onClicked: effects.markAsDashboard(dialogItem)
            }
            PlasmaComponents.Button {
                text: "Thumbnail"
                checkable: true
                onClicked: {
                    if (checked && !dialogButton.checked) {
                        dialogButton.checked = true;
                    }
                    if (checked) {
                        effects.thumbnail(dialogItem, buttons, Qt.rect(10, 10, 180, 180));
                    } else {
                        effects.thumbnail(dialogItem);
                    }
                }
            }
        }
        PlasmaComponents.ButtonRow {
            PlasmaComponents.Button {
                text: "Slide Dialog from Top"
                onClicked: effects.slideWindow(dialogItem, KWindowEffects.TopEdge)
            }
            PlasmaComponents.Button {
                text: "Slide Dialog from Left"
                onClicked: effects.slideWindow(dialogItem, KWindowEffects.LeftEdge)
            }
            PlasmaComponents.Button {
                text: "Slide Dialog from Right"
                onClicked: effects.slideWindow(dialogItem, KWindowEffects.RightEdge)
            }
            PlasmaComponents.Button {
                text: "Slide Dialog from Bottom"
                onClicked: effects.slideWindow(dialogItem, KWindowEffects.BottomEdge)
            }
        }
        PlasmaComponents.ButtonRow {
            PlasmaComponents.Button {
                text: "Present Windows (all)"
                onClicked: effects.presentWindowsAll(buttons)
            }
            PlasmaComponents.Button {
                text: "Present Windows (current)"
                onClicked: effects.presentWindowsCurrent(buttons)
            }
        }
        PlasmaComponents.Button {
            checkable: true
            text: "Highlight Windows"
            onClicked: effects.highlightWindows(buttons, checked);
        }
    }
}
