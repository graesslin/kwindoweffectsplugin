/*
 *   Copyright 2013 by Martin Gräßlin <mgraesslin@kde.org>

 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU Library General Public License as
 *   published by the Free Software Foundation; either version 2, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details
 *
 *   You should have received a copy of the GNU Library General Public
 *   License along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
#ifndef KWINDOWEFFECTSPLUGIN_MODEL_H
#define KWINDOWEFFECTSPLUGIN_MODEL_H

#include <QAbstractItemModel>
#include <QRect>

class QQuickItem;

class Model : public QAbstractItemModel
{
    Q_OBJECT
    Q_ENUMS(SlideFromLocation)
public:
    enum SlideFromLocation {
        NoEdge = 0,
        TopEdge,
        RightEdge,
        BottomEdge,
        LeftEdge
    };
    explicit Model(QObject* parent = 0);
    virtual ~Model();
    virtual int columnCount(const QModelIndex& parent = QModelIndex()) const;
    virtual QVariant data(const QModelIndex& index, int role = Qt::DisplayRole) const;
    virtual QModelIndex index(int row, int column, const QModelIndex& parent = QModelIndex()) const;
    virtual QModelIndex parent(const QModelIndex& child) const;
    virtual int rowCount(const QModelIndex& parent = QModelIndex()) const;
    virtual QHash< int, QByteArray > roleNames() const;

    Q_INVOKABLE void presentWindowsAll(QQuickItem *item);
    Q_INVOKABLE void presentWindowsCurrent(QQuickItem *item);
    Q_INVOKABLE void highlightWindows(QQuickItem *controller, bool enable);
    Q_INVOKABLE void blurBehind(QQuickItem *item, bool enable);
    Q_INVOKABLE void markAsDashboard(QQuickItem *item);
    Q_INVOKABLE void slideWindow(QQuickItem *item, SlideFromLocation location, int offset = 0);
    Q_INVOKABLE void thumbnail(QQuickItem *controller, QQuickItem *window = nullptr, const QRect &rect = QRect());

private:
    QString indexToName(int index) const;
    bool indexToEffect(int index) const;
};

#endif // KWINDOWEFFECTSPLUGIN_MODEL_H
